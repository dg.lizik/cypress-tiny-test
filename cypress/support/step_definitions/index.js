import { Given as given, Then as then } from 'cypress-cucumber-preprocessor/steps';

given('I am {string}', function(username) {
  this.username = username;
});

given('I am on the homepage', () => {
  cy.visit('/');
});

then('I set the number of people to {string}', (str) => {
  cy.get('.people-select select').select(str);
});

then('I set the time to {string}', (time) => {
  cy.get('.time-select select').select(time);
});

then('it should work', () => {
  cy.get('.icon-tc-logo-color-svg').should('have.length', 1);
});
